<?php
/**
 * @file
 * announcement.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function announcement_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function announcement_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function announcement_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: announcement_1
  $nodequeues['announcement_1'] = array(
    'name' => 'announcement_1',
    'title' => 'Announcement 1',
    'subqueue_title' => '',
    'size' => 1,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'announcement',
      1 => 'article',
      2 => 'found_pet',
      3 => 'lost_pet',
      4 => 'news',
      5 => 'pet_for_adoption',
      6 => 'photo_contest',
      7 => 'video_contest',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: announcement_2
  $nodequeues['announcement_2'] = array(
    'name' => 'announcement_2',
    'title' => 'Announcement 2',
    'subqueue_title' => '',
    'size' => 1,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'announcement',
      1 => 'article',
      2 => 'found_pet',
      3 => 'lost_pet',
      4 => 'news',
      5 => 'pet_for_adoption',
      6 => 'photo_contest',
      7 => 'video_contest',
    ),
    'roles' => array(),
    'count' => 0,
  );

  // Exported nodequeues: announcement_3
  $nodequeues['announcement_3'] = array(
    'name' => 'announcement_3',
    'title' => 'Announcement 3',
    'subqueue_title' => '',
    'size' => 1,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'announcement',
      1 => 'article',
      2 => 'found_pet',
      3 => 'lost_pet',
      4 => 'news',
      5 => 'pet_for_adoption',
      6 => 'photo_contest',
      7 => 'video_contest',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_node_info().
 */
function announcement_node_info() {
  $items = array(
    'announcement' => array(
      'name' => t('Announcement'),
      'base' => 'node_content',
      'description' => t('For making one-off announcements on the front page'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
